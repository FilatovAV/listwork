﻿using System;
using System.Collections.Generic;

namespace ListWork
{
    internal class ListWork
    {
        public static void Do()
        {
            var rnd = new Random();
            var stringList = new List<string>();
            for (var i = 0; i < 100; i++)
            {
                stringList.Add(((float)rnd.Next(20, 2200) / 10f).ToString());
            }

            foreach (var item in stringList)
            {
                Console.WriteLine(item);
            }

            Console.ForegroundColor = ConsoleColor.Yellow;
            var max = stringList.GetMax<string>(x => float.TryParse(x, out float parsedVal) ? parsedVal : default(float));

            Console.WriteLine($"Максимальное значение в списке: {max}.");
            Console.Read();
        }
    }
}