﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ListWork
{
    public static class Ext
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter) where T : class
        {
            var max = e.Max(m => getParameter?.Invoke(m));
            var res = e.FirstOrDefault(f => getParameter?.Invoke(f) == max);
            return res;
        }
    }
}